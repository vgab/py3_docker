# Docker container for basic python3 environments

This Repo contains Dockerfiles which are available in [Dockerhub/volkg/py3_slim](https://hub.docker.com/r/volkg/py3_slim/). 
A short explanation about the build procedure is given below.


### Build Docker images

1. Base Installation:
	```
	docker build -t volkg/py3_slim:latest -f py_slim_base.Dockerfile .
	```

2. Graph tool Extension:
	```
	docker build -t volkg/py3_slim:graphs -f py_slim_graph_toll.Dockerfile .
	```

3. Tensorflow Extension (under construction)

### push to Dockerhub

```
docker login 
[...]
docker push volkg/py3_slim:[TAG]
```


#### Sources
These Docker-Images are extensions/modifications of the docker containers below:
[frolvlad/alpine-python3](https://hub.docker.com/r/frolvlad/alpine-python3/~/dockerfile/)
[frol/docker-alpine-python-machine-learning](https://github.com/frol/docker-alpine-python-machinelearning/blob/master/Dockerfile)
[fgrehm/docker-graphviz](https://github.com/fgrehm/docker-graphviz)
